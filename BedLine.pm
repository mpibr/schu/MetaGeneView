package BedLine;

sub new 
{
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
}

sub chrom 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{chrom} = $value if defined($value);
    return $self->{chrom};
}

sub chromStart 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{chromStart} = $value if defined($value);
    return $self->{chromStart};
}

sub chromEnd 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{chromEnd} = $value if defined($value);
    return $self->{chromEnd};
}

sub name 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{name} = $value if defined($value);
    return $self->{name};
}

sub score 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{score} = $value if defined($value);
    return $self->{score};
}

sub strand 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{strand} = $value if defined($value);
    return $self->{strand};
}

sub thickStart 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{thickStart} = $value if defined($value);
    return $self->{thickStart};
}

sub thickEnd 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{thickEnd} = $value if defined($value);
    return $self->{thickEnd};
}

sub itemRgb 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{itemRgb} = $value if defined($value);
    return $self->{itemRgb};
}

sub blocks 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{blocks} = $value if defined($value);
    return $self->{blocks};
}

sub blockSizes 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{blockSizes} = $value if defined($value);
    return $self->{blockSizes};
}

sub blockStarts 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{blockStarts} = $value if defined($value);
    return $self->{blockStarts};
}

sub cdsStart 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{cdsStart} = $value if defined($value);
    return $self->{cdsStart};
}

sub cdsEnd 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{cdsEnd} = $value if defined($value);
    return $self->{cdsEnd};
}

sub cdsSpan
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{cdsSpan} = $value if defined($value);
    return $self->{cdsSpan};
}

sub geneSpan 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{geneSpan} = $value if defined($value);
    return $self->{geneSpan};
}

sub exonStart 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{exonStart} = $value if defined($value);
    return $self->{exonStart};
}

sub exonEnd 
{
    my $self = $_[0]; 
    my $value = $_[1];
    $self->{exonEnd} = $value if defined($value);
    return $self->{exonEnd};
}

sub parse
{
    my $self = $_[0];
    my $line = $_[1];

    my @bedLine = split("\t", $_, 12);
    $self->chrom($bedLine[0]);
    $self->chromStart($bedLine[1]);
    $self->chromEnd($bedLine[2]);
    $self->name($bedLine[3]);
    $self->score($bedLine[4]);
    $self->strand($bedLine[5]);
    $self->thickStart($bedLine[6]);
    $self->thickEnd($bedLine[7]);
    $self->itemRgb($bedLine[8]);
    $self->blocks($bedLine[9]);
    my @blockSizes = split(",", $bedLine[10]);
    my @blockStarts = split(",", $bedLine[11]);
    $self->blockSizes(\@blockSizes);
    $self->blockStarts(\@blockStarts);

    $self->exons();
}

sub exons
{
    my $self = $_[0];
    my $offset = 0;
    my @listExonStart = ();
    my @listExonEnd = ();
    for (my $e = 0; $e < $self->blocks; $e++)
    {
        my $exonStart = $self->chromStart + $self->blockStarts->[$e];
        my $exonEnd = $exonStart + $self->blockSizes->[$e];
        push(@listExonStart, $exonStart);
        push(@listExonEnd, $exonEnd);

        if (($exonStart <= $self->thickStart) && ($self->thickStart <= $exonEnd))
        {
            $self->cdsStart($self->thickStart - $exonStart + $offset);
        }

        if (($exonStart <= $self->thickEnd) && ($self->thickEnd <= $exonEnd))
        {
            $self->cdsEnd($self->thickEnd - $exonStart + $offset);
        }

        $offset += $self->blockSizes->[$e];
    }

    $self->geneSpan($offset);
    $self->cdsSpan($self->cdsEnd - $self->cdsStart);
    $self->exonStart(\@listExonStart);
    $self->exonEnd(\@listExonEnd);
}

sub find
{
    my $self = $_[0];
    my $rangeStart = $_[1];
    my $rangeEnd = $_[2];

    my $offset = 0;
    my @result = ();
    for (my $e = 0; $e < $self->{blocks}; $e++)
    {
        # current exon
        my $exonStart = $self->{chromStart} + $self->{blockStarts}->[$e];
        my $exonEnd = $exonStart + $self->{blockSizes}->[$e];
        
        # check for overlap
        if(($rangeStart <= $exonEnd) && ($exonStart <= $rangeEnd))
        {
            # intersect between region and exon
            my $isectStart = max($rangeStart, $exonStart);
            my $isectEnd = min($rangeEnd, $exonEnd);

            # convert to linear coordinates
            my $linearStart = $isectStart - $exonStart + $offset;
            my $linearEnd = $isectEnd - $exonStart + $offset;

            push(@result, [$linearStart, $linearEnd]);
        }
        
        # update offset from chromStart
        $offset += $self->{blockSizes}->[$e];
    }

    return \@result;
}

sub min
{
    my $x = $_[0];
    my $y = $_[1];

    return ($x <= $y) ? $x : $y;
}

sub max
{
    my $x = $_[0];
    my $y = $_[1];
    return ($x <= $y) ? $y : $x;
}

1;