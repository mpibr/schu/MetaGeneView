# MetaGeneView
create a Genome Browser Track of linearised genomic coverage (removing introns).

## create coverage from BAM files

The script scales the read to represent the coverage as reads per million [RPMs].

```
perl BatchCoverage.pl config.txt /path/to/bams/ /path/to/gbed/ tag
```

```
# config is TAB delimited file
cat config.txt
file_bam group rpm
```

## create a track file

```
perl MetaGeneView.pl --bed /path/to/annotation/ncbiRefSeq_rn6_Sep2018.bed --reps 3 --gbed $(ls /path/to/gbed/*.gbed.gz) --query "Camk2a" > myCustomTrack.txt
```

* note: use double quotes to escape special characted in query gene symbol

## help

```
$ perl MetaGeneView.pl -h

MetageneView Version 1.0
usage: MetaGeneView.pl --bed annotation.bed --gbed runA.gbed.gz runB.gbed.gz --query GeneOfInterest
description: MetageneView creates a BedGraph track file with linear gene representation (no introns).
parameters:
-q|--query
        query gene name
-r|--reps
        number of replica, to accumulate coverage in consecutive files
-b|--bed
        valid annotation file in BED12 format
-g|--gbed
        single or a space-separated list of GraphBed files.
        The GraphBed files need to be block compressed with BGZIP.
        The script uses Tabix to query the coverage files.
-help
        define usage

```